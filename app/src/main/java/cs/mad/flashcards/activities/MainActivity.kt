package cs.mad.flashcards.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityMainBinding
import cs.mad.flashcards.entities.*
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val flashcardSetDao by lazy { FlashcardsDatabase.getDatabase(applicationContext).flashcardSetDao() }
    private lateinit var flashcardSetService: FlashcardSetService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        flashcardSetService = Retrofit.Builder()
            .baseUrl("http://localhost/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(FlashcardSetService::class.java)

        binding.flashcardSetList.adapter = FlashcardSetAdapter(listOf(), flashcardSetDao)

        loadSets()

        binding.createSetButton.setOnClickListener {
            lifecycleScope.launch {
                flashcardSetDao.insert(FlashcardSet(null, null, "test"))
                loadFromDb()
                binding.flashcardSetList.smoothScrollToPosition((binding.flashcardSetList.adapter as FlashcardSetAdapter).itemCount - 1)
            }

        }
    }

    private fun loadSets() {
        lifecycleScope.launch {
            loadFromDb()
        }
    }


    private fun loadFromDb() {
        lifecycleScope.launch {
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).setData(flashcardSetDao.getAll())
        }
    }
}