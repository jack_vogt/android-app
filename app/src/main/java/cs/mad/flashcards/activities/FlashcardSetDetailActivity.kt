package cs.mad.flashcards.activities

import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.Observer
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.entities.*
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFlashcardSetDetailBinding
    private val flashcardDao by lazy { FlashcardsDatabase.getDatabase(applicationContext).flashcardDao() }
    private lateinit var flashcardService: FlashcardService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        flashcardService = Retrofit.Builder()
            .baseUrl("http://localhost")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(FlashcardService::class.java)

        binding.flashcardList.adapter = FlashcardAdapter(listOf(), flashcardDao)

        loadFlashcards()

        binding.addFlashcardButton.setOnClickListener {
            lifecycleScope.launch {
                flashcardDao.insert(Flashcard(null, null, "new term", "new definition"))
                loadFromDb()
            }
            binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardAdapter).itemCount - 1)
        }

        binding.studySetButton.setOnClickListener {
            startActivity(Intent(this, StudySetActivity::class.java))
        }

        binding.deleteSetButton.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    fun addFlashcard(flashcard: Flashcard) {
        lifecycleScope.launch {
            flashcardDao.insert(flashcard)
            loadFromDb()
        }
    }

    fun updateFlashcard(flashcard: Flashcard) {
        lifecycleScope.launch {
            flashcardDao.update(flashcard)
            loadFromDb()
        }
    }

    private fun loadFlashcards() {
        lifecycleScope.launch {
            loadFromDb()
        }
    }

    private fun loadFromDb() {
        lifecycleScope.launch {
            (binding.flashcardList.adapter as FlashcardAdapter).setData(flashcardDao.getAll())
        }

    }
}