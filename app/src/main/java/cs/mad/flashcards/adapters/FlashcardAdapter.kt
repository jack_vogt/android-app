package cs.mad.flashcards.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardDao
import cs.mad.flashcards.entities.FlashcardSet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class FlashcardAdapter(private var dataSet: List<Flashcard>, private val dao: FlashcardDao) :
    RecyclerView.Adapter<FlashcardAdapter.ViewHolder>() {

    init {
        setData(dataSet)
    }

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val flashcardTitle: TextView = view.findViewById(R.id.flashcard_title)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard, viewGroup, false)
        )
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.flashcardTitle.text = item.term
        viewHolder.itemView.setOnClickListener {
            AlertDialog.Builder(it.context)
                    .setTitle(item.term)
                    .setMessage(item.definition)
                    .setPositiveButton("Done") { _,_ -> }
                    .setNeutralButton("Edit") { _, _ -> showEditDialog(it.context, item) }
                    .create()
                    .show()
        }
        viewHolder.itemView.setOnLongClickListener {
            showEditDialog(it.context, item)
            true
        }
    }

    private fun showEditDialog(context: Context, flashcard: Flashcard) {
        val customTitle = EditText(context)
        val customBody = EditText(context)
        customTitle.setText(flashcard.definition)
        customTitle.textSize = 20 * context.resources.displayMetrics.scaledDensity
        customBody.setText(flashcard.term)
        AlertDialog.Builder(context)
                .setCustomTitle(customTitle)
                .setView(customBody)
                .setPositiveButton("Done") { _,_ ->
                    GlobalScope.launch {
                        flashcard.term = customTitle.text.toString()
                        flashcard.definition = customBody.text.toString()
                        dao.update(flashcard)
                        GlobalScope.launch(Dispatchers.Main) {
                            setData(dao.getAll())
                        }
                    }

                }
                .setNegativeButton("Delete") { _,_ ->
                    GlobalScope.launch {
                        dao.delete(flashcard)
                        GlobalScope.launch(Dispatchers.Main) {
                            setData(dao.getAll())
                        }
                    }
                }
                .create()
                .show()
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

//    fun addItem(it: Flashcard) {
//        dataSet.add(it)
//        notifyItemInserted(dataSet.lastIndex)
//    }

    fun setData(items: List<Flashcard>) {
        dataSet = items
        notifyDataSetChanged()
    }
}