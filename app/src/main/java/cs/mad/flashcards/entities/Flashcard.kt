package cs.mad.flashcards.entities

import androidx.room.*
import com.google.gson.annotations.SerializedName
import retrofit2.Call
import retrofit2.http.GET

data class WebFlashcards(val flashcards: List<Flashcard>)

@Entity
data class Flashcard(@PrimaryKey(autoGenerate = true) val myId: Long?, @SerializedName("id") val outsideId: Long?, var term: String, var definition: String)

@Dao
interface FlashcardDao {
    @Query("SELECT * FROM flashcard")
    suspend fun getAll(): List<Flashcard>

    @Query("DELETE FROM flashcard WHERE myId NOT NULL")
    suspend fun deleteFromWeb()

    @Insert
    suspend fun insert(vararg flashcard: Flashcard)

    @Insert
    suspend fun insert(flashcard: List<Flashcard>)

    @Update
    suspend fun update(flashcard: Flashcard)

    @Delete
    suspend fun delete(flashcard: Flashcard)
}

interface FlashcardService {
    @GET("flashcards")
    fun getAll(): Call<WebFlashcards>
}


//data class Flashcard(
//    val question: String,
//    val answer: String
//)

fun getHardcodedFlashcards(): List<Flashcard> {
    return listOf(Flashcard(null, null,"Term 1", "Def 1"),
        Flashcard( null, null,"Term 2", "Def 2"),
        Flashcard( null, null,"Term 3", "Def 3"),
        Flashcard( null, null,"Term 4", "Def 4"),
        Flashcard( null, null,"Term 5", "Def 5"),
        Flashcard( null, null,"Term 6", "Def 6"),
        Flashcard( null, null,"Term 7", "Def 7"),
        Flashcard( null, null,"Term 8", "Def 8"),
        Flashcard( null, null,"Term 9", "Def 9"),
        Flashcard( null, null,"Term 10", "Def 10")
    )
}