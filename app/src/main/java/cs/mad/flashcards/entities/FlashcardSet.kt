package cs.mad.flashcards.entities

import androidx.room.*
import com.google.gson.annotations.SerializedName
import retrofit2.Call
import retrofit2.http.GET

data class WebFlashcardSets(val flashcardSets: List<FlashcardSet>)

@Entity
data class FlashcardSet(@PrimaryKey(autoGenerate = true) val myId: Long?, @SerializedName("id") val outsideId: Long?, val title: String)

@Dao
interface FlashcardSetDao {
    @Query("SELECT * FROM flashcardSet")
    suspend fun getAll(): List<FlashcardSet>

    @Query("DELETE FROM flashcardSet WHERE myId NOT NULL")
    suspend fun deleteFromWeb()

    @Insert
    suspend fun insert(vararg flashcardSet: FlashcardSet)

    @Insert
    suspend fun insert(flashcardSets: List<FlashcardSet>)

    @Update
    suspend fun update(flashcardSet: FlashcardSet)

    @Delete
    suspend fun delete(flashcardSet: FlashcardSet)
}

interface FlashcardSetService {
    @GET("flashcardSets")
    fun getAll(): Call<WebFlashcardSets>
}



//data class FlashcardSet(val title: String, val id: Long? = null)
//
fun getHardcodedFlashcardSets(): List<FlashcardSet> {
    return mutableListOf(FlashcardSet(null, null, "Set 1"),
        FlashcardSet( null, null, "Set 2"),
        FlashcardSet( null, null, "Set 3"),
        FlashcardSet( null, null, "Set 4"),
        FlashcardSet( null, null, "Set 5"),
        FlashcardSet( null, null, "Set 6"),
        FlashcardSet( null, null, "Set 7"),
        FlashcardSet( null, null, "Set 8"),
        FlashcardSet( null, null, "Set 9"),
        FlashcardSet( null, null, "Set 10")
    )
}